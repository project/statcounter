
AUTHOR
------
Jimmy Berry ("boombatower", http://drupal.org/user/214218)

PROJECT PAGE
------------
If you need more information, have an issue, or feature request please
visit the project page at: http://drupal.org/project/statcounter.

DESCRIPTION
-----------
This module takes advantage of the block system. Because of that you
can take advantage of the block system's scope fields and define more
complicated scope then defaulty provided by the module.

To do so goto admin/build/block find the statcount block and click
configure. Near the bottom you will see fields for specifying page
visibility.

Make sure you set the statcounter module scope to all so that it
doesn't override any block settings.
